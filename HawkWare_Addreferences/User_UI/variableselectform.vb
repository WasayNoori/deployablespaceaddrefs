﻿Imports EdmLib

Public Class variableselectform
    Private _vault As EdmVault5
    Private _selvariables As List(Of String)

    Sub New(vault As EdmVault5)
        _vault = vault

        InitializeComponent()

    End Sub
    Private Sub variableselectform_Load(sender As Object, e As EventArgs) Handles MyBase.Load
     

    End Sub
    Public Property selectedVariables As List(Of String)
        Get
            getselected()

            Return _selvariables

        End Get
        Set(value As List(Of String))
            _selvariables = New List(Of String)
            _selvariables = value


        End Set
    End Property
    Private Sub variableselectform_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim varList As New List(Of IEdmVariable5)
        Dim pdmlist As New EpdmLists(_vault)

        varList = pdmlist.getVarList
        CheckedListBox1.Items.Clear()

        For Each v As IEdmVariable5 In varList
            CheckedListBox1.Items.Add(v.Name)

        Next

        setVars()
    End Sub
    Private Sub getselected()
        _selvariables = New List(Of String)

        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            If CheckedListBox1.GetItemCheckState(i) = Windows.Forms.CheckState.Checked Then
                _selvariables.Add(CheckedListBox1.Items(i).ToString)
            End If
        Next
    End Sub
    Private Sub setVars()
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            If _selvariables.Contains(CheckedListBox1.Items(i).ToString) Then
                CheckedListBox1.SetItemCheckState(i, Windows.Forms.CheckState.Checked)
            End If
        Next
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()

    End Sub
End Class