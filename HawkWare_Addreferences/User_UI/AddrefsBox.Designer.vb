﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddrefsBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbFiles = New System.Windows.Forms.ListBox()
        Me.lbSources = New System.Windows.Forms.ListBox()
        Me.lbexistingfiles = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblext = New System.Windows.Forms.Label()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.TbComment = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbCheckin = New System.Windows.Forms.CheckBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbFiles
        '
        Me.lbFiles.FormattingEnabled = True
        Me.lbFiles.HorizontalScrollbar = True
        Me.lbFiles.Location = New System.Drawing.Point(27, 75)
        Me.lbFiles.Name = "lbFiles"
        Me.lbFiles.Size = New System.Drawing.Size(184, 225)
        Me.lbFiles.TabIndex = 0
        '
        'lbSources
        '
        Me.lbSources.FormattingEnabled = True
        Me.lbSources.Location = New System.Drawing.Point(404, 75)
        Me.lbSources.Name = "lbSources"
        Me.lbSources.Size = New System.Drawing.Size(202, 134)
        Me.lbSources.TabIndex = 2
        '
        'lbexistingfiles
        '
        Me.lbexistingfiles.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lbexistingfiles.FormattingEnabled = True
        Me.lbexistingfiles.HorizontalScrollbar = True
        Me.lbexistingfiles.Location = New System.Drawing.Point(217, 75)
        Me.lbexistingfiles.Name = "lbexistingfiles"
        Me.lbexistingfiles.Size = New System.Drawing.Size(172, 225)
        Me.lbexistingfiles.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(214, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Existing Reference"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(401, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(202, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Source Document (To copy properties)"
        '
        'lblext
        '
        Me.lblext.AutoSize = True
        Me.lblext.Location = New System.Drawing.Point(556, 9)
        Me.lblext.Name = "lblext"
        Me.lblext.Size = New System.Drawing.Size(104, 13)
        Me.lblext.TabIndex = 8
        Me.lblext.Text = "Source Extension:  "
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(630, 338)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 9
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'TbComment
        '
        Me.TbComment.Location = New System.Drawing.Point(404, 267)
        Me.TbComment.Multiline = True
        Me.TbComment.Name = "TbComment"
        Me.TbComment.Size = New System.Drawing.Size(301, 55)
        Me.TbComment.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(401, 239)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Check-in Comment"
        '
        'cbCheckin
        '
        Me.cbCheckin.AutoSize = True
        Me.cbCheckin.Checked = True
        Me.cbCheckin.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCheckin.Location = New System.Drawing.Point(559, 239)
        Me.cbCheckin.Name = "cbCheckin"
        Me.cbCheckin.Size = New System.Drawing.Size(92, 17)
        Me.cbCheckin.TabIndex = 12
        Me.cbCheckin.Text = "Check-in File"
        Me.cbCheckin.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.BackgroundImage = Global.HawkWare_Addreferences.My.Resources.Resources.delete_property_26
        Me.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Location = New System.Drawing.Point(80, 46)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(20, 20)
        Me.btnClear.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.btnClear, "Remove All")
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.BackgroundImage = Global.HawkWare_Addreferences.My.Resources.Resources.delete_26
        Me.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemove.Location = New System.Drawing.Point(54, 46)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(20, 20)
        Me.btnRemove.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.btnRemove, "Remove Selected File")
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackgroundImage = Global.HawkWare_Addreferences.My.Resources.Resources.add_file_26
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Location = New System.Drawing.Point(28, 46)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(20, 20)
        Me.btnAdd.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.btnAdd, "Add Reference")
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(170, 15)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "HawkWare™ Add References"
        '
        'AddrefsBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(738, 375)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cbCheckin)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TbComment)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.lblext)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbexistingfiles)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.lbSources)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.lbFiles)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AddrefsBox"
        Me.Text = "HawkWare™ Add References"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbFiles As System.Windows.Forms.ListBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lbSources As System.Windows.Forms.ListBox
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents lbexistingfiles As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblext As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents TbComment As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbCheckin As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
