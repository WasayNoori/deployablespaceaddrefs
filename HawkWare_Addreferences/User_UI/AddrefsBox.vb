﻿Imports EdmLib
Imports System.Windows.Forms

Public Class AddrefsBox
    Dim _Vault As EdmVault5
    Dim _files As List(Of FileAndFolder)
    Dim _file As IEdmFile8
    Dim _extn As String
    Dim _targetFiles As List(Of IEdmFile8)
    Dim _fileIDs As List(Of Integer)
    Dim _existingFiles As List(Of FileAndFolder)
    Dim _existinfFileIDs As List(Of Integer)
    Dim _sourcefile As IEdmFile8
    Dim _checkin As Boolean

    Public WriteOnly Property targetExtension As String
        Set(value As String)
            _extn = value

        End Set
    End Property
    Public WriteOnly Property Existinglist As List(Of FileAndFolder)
        Set(value As List(Of FileAndFolder))
            _existingFiles = New List(Of FileAndFolder)
            _existingFiles = value
            _existinfFileIDs = New List(Of Integer)
            For Each temp As FileAndFolder In _existingFiles
                _existinfFileIDs.Add(temp.FileID)
            Next

        End Set

    End Property
    Public ReadOnly Property FileList As List(Of FileAndFolder)
        Get
            Return _files

        End Get
    End Property
    Public ReadOnly Property sourcefile As IEdmFile8
        Get
            Return _sourcefile
        End Get
    End Property
    Public ReadOnly Property comment As String
        Get
            Return TbComment.Text

        End Get
    End Property
    Public ReadOnly Property checkfile As Boolean
        Get
            Return _checkin
        End Get
    End Property
    Sub New(vault As EdmVault5, file As IEdmFile8)
        InitializeComponent()
        _Vault = vault
        _file = file


    End Sub
    Private Sub AddrefsBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _files = New List(Of FileAndFolder)
        _fileIDs = New List(Of Integer)

        _targetFiles = New List(Of IEdmFile8)
    End Sub


    Private Function BrowseFiles(vault As EdmVault5, parentWnd As Long) As List(Of FileAndFolder)
        Dim retlist As New List(Of FileAndFolder)
        Dim file As IEdmFile8
        Dim fold As IEdmFolder5
        Dim ff As FileAndFolder
        'Let the user select one or more text files that must be part of the
        'file vault to which we are logged in.
        Dim PathList As EdmStrLst5
        PathList = vault.BrowseForFile(parentWnd, _
                                           EdmBrowseFlag.EdmBws_ForOpen + EdmBrowseFlag.EdmBws_PermitMultipleSel + EdmBrowseFlag.EdmBws_PermitVaultFiles)



        'Check if the user pressed Cancel
        If PathList Is Nothing Then
            Return retlist
        Else
            'Display a message box with the paths of all selected files.
            Dim message As String
            message = "You selected the following files:" + vbLf
            Dim pos As IEdmPos5
            pos = PathList.GetHeadPosition
            While Not pos.IsNull

                Try
                    ff = New FileAndFolder
                    file = vault.GetFileFromPath(PathList.GetNext(pos), fold)

                    If Not file Is Nothing Then
                        ff.FileID = file.ID
                        ff.FoldID = fold.ID

                        retlist.Add(ff)

                    End If
                Catch ex As Exception
                    ErrorLogger.LogException(vault, ex, "", "", "adding file to references", "")
                End Try

            End While

        End If
        Return retlist

    End Function


    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim templist As New List(Of FileAndFolder)
        templist = BrowseFiles(_Vault, Me.Handle)

        For Each temp As FileAndFolder In templist
            Try
                If Not _fileIDs.Contains(temp.FileID) AndAlso Not (_existinfFileIDs.Contains(temp.FileID)) AndAlso Not temp.FileID = _file.ID Then
                    _files.Add(temp)
                    _fileIDs.Add(temp.FileID)
                End If
            Catch ex As Exception
            End Try

        Next
        refreshList()

    End Sub

    Private Sub refreshList()
        Dim thisfile As IEdmFile8
        Try

            For Each f As FileAndFolder In _files
                Try
                    thisfile = _Vault.GetObject(EdmObjectType.EdmObject_File, f.FileID)
                    If Not thisfile Is Nothing AndAlso Not lbFiles.Items.Contains(thisfile.Name) Then
                        lbFiles.Items.Add(thisfile.Name)

                    End If
                Catch ex As Exception
                End Try

            Next
        Catch mainEx As Exception
        End Try

        refreshTargets()

    End Sub
    Private Sub refreshTargets()
        Dim thisfile As IEdmFile8
        Try
            _targetFiles = New List(Of IEdmFile8)
            If lbSources.Items.Count > 0 Then
                lbSources.Items.Clear()
            End If
            For Each f As FileAndFolder In _files
                Try
                    thisfile = _Vault.GetObject(EdmObjectType.EdmObject_File, f.FileID)
                    If Not thisfile Is Nothing AndAlso Not (String.IsNullOrEmpty(_extn)) AndAlso IO.Path.GetExtension(thisfile.Name).ToLower = _extn.ToLower Then
                        _targetFiles.Add(thisfile)
                        lbSources.Items.Add(thisfile.Name)
                    End If
                Catch ex2 As Exception
                End Try

            Next

        Catch ex As Exception
        End Try


    End Sub
    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Dim selind As Integer
        Dim thisfile As IEdmFile8
        selind = lbFiles.SelectedIndex

        If Not selind < 0 Then

            _files.RemoveAt(selind)

            lbFiles.Items.RemoveAt(selind)
          
            refreshList()

        End If
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        _files.Clear()
        _fileIDs.Clear()

        If lbFiles.Items.Count > 0 Then
            lbFiles.Items.Clear()

        End If


        refreshList()

    End Sub

    Private Sub AddrefsBox_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim thisfile As IEdmFile8
        lblext.Text = lblext.Text & _extn
        If Not _existingFiles Is Nothing Then
            For Each ff As FileAndFolder In _existingFiles
                Try
                    thisfile = _Vault.GetObject(EdmObjectType.EdmObject_File, ff.FileID)
                    If Not thisfile Is Nothing Then
                        lbexistingfiles.Items.Add(thisfile.Name)
                    End If
                Catch ex As Exception

                End Try


            Next
        End If

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If lbSources.Items.Count = 0 Then
            If MessageBox.Show("No source file selected for copying variables. Continue? ", "", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
                Exit Sub

            End If
        End If

        If lbSources.Items.Count > 1 AndAlso lbSources.SelectedIndex < 0 Then
            MessageBox.Show("Select a source file to copy variables from. ")
            Exit Sub
        End If

        If lbSources.Items.Count = 1 Then
            lbSources.SelectedIndex = 0
        End If


        If lbSources.SelectedIndex >= 0 Then
            _sourcefile = _targetFiles.Item(lbSources.SelectedIndex)

        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

  
    Private Sub cbCheckin_CheckedChanged(sender As Object, e As EventArgs) Handles cbCheckin.CheckedChanged
        If cbCheckin.Checked Then
            TbComment.Enabled = True
            _checkin = True

        Else
            TbComment.Enabled = False
            _checkin = False
        End If
    End Sub
End Class