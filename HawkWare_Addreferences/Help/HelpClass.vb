﻿Imports System.Reflection
Imports System.IO
Imports System.Windows.Forms

Public Class HelpClass
    Public Sub launchPDF()
        Dim _imageStream As Stream
        Dim _textStreamReader As StreamReader
        Dim _assembly As [Assembly]
        Dim tempFold As String
        tempFold = IO.Path.GetTempPath
        Try
            If IO.File.Exists(IO.Path.Combine(tempFold & "HawkWare_AddReferences_Guide.PDF")) Then
                Diagnostics.Process.Start(IO.Path.Combine(tempFold & "HawkWare_AddReferences_Guide.PDF"))
            End If
            Exit Sub
        Catch ex As Exception

        End Try
        Try
            Dim names() As String = [Assembly].GetExecutingAssembly.GetManifestResourceNames

           
            _assembly = [Assembly].GetExecutingAssembly()

            _imageStream = _assembly.GetManifestResourceStream("HawkWare_AddReferences.HawkWare_AddReferences_Guide.pdf")

            Dim bytes(_imageStream.Length) As Byte
            _imageStream.Read(bytes, 0, bytes.Length)
            My.Computer.FileSystem.WriteAllBytes(IO.Path.Combine(tempFold & "HawkWare_AddReferences_Guide.PDF"), bytes, False)
            Diagnostics.Process.Start(IO.Path.Combine(tempFold & "HawkWare_AddReferences_Guide.PDF"))


        Catch ex As Exception
            MessageBox.Show("Error launching Help file. " & ex.Message, "Error")
        End Try
    End Sub
End Class
