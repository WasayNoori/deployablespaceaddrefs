﻿Imports EdmLib
Imports System.Windows.Forms

Public Class Class1

    Implements IEdmAddIn5

    Public Sub GetAddInInfo(ByRef poInfo As EdmLib.EdmAddInInfo, ByVal poVault As EdmLib.IEdmVault5, ByVal poCmdMgr As EdmLib.IEdmCmdMgr5) Implements EdmLib.IEdmAddIn5.GetAddInInfo

        'Return information about this add-in to SolidWorks Enterprise PDM in the struct
        poInfo.mbsAddInName = My.Application.Info.ProductName

        poInfo.mbsCompany = My.Application.Info.CompanyName


        poInfo.mbsDescription = My.Application.Info.Description

        poInfo.mlAddInVersion = 1
        poInfo.mlRequiredVersionMajor = 13
        poInfo.mlRequiredVersionMinor = 0


        'Call me when a file has been added
        'poCmdMgr.AddHook(EdmCmdType.EdmCmd_PostAdd)

        ''Call me when a file has been checked out
        'poCmdMgr.AddHook(EdmCmdType.EdmCmd_PostLock)

        ''Call me when a file is about to be checked in
        'poCmdMgr.AddHook(EdmCmdType.EdmCmd_PreUnlock)

        ''Call me when a file has been checked in
        'poCmdMgr.AddHook(EdmCmdType.EdmCmd_PostUnlock)

        poCmdMgr.AddCmd(1, "test fire hello", EdmMenuFlags.EdmMenu_Nothing)

        poCmdMgr.AddCmd(2, "Settings", EdmMenuFlags.EdmMenu_Administration)
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_CardButton)

    End Sub


    Public Sub OnCmd(ByRef poCmd As EdmLib.EdmCmd, ByRef ppoData As System.Array) Implements EdmLib.IEdmAddIn5.OnCmd

        'Check the kind of hook that was called.
        Dim name As String
        Dim vault As New EdmVault5
        Dim file As IEdmFile8
        Dim curUser As IEdmUser10 = Nothing
 

        vault = poCmd.mpoVault
        Dim pdmClass As New EpdmLists(vault)
        curUser = pdmClass.currentuser
        Select Case poCmd.meCmdType
            '    Case EdmCmdType.EdmCmd_PostAdd
            '        name = "PostAdd"
            '    Case EdmCmdType.EdmCmd_PostLock
            '        name = "PostLock"
            '    Case EdmCmdType.EdmCmd_PreUnlock
            '        name = "PreUnlock"
            '    Case EdmCmdType.EdmCmd_PostUnlock
            '        name = "PostUnlock"
            '    Case Else
            '        name = "?"
            Case EdmCmdType.EdmCmd_CardButton
            
                Dim commandCOm As String
                commandCOm = poCmd.mbsComment


                Dim ext As String
                If (commandCOm IsNot Nothing) AndAlso commandCOm.ToLower.StartsWith("addref") Then

                    If Not commandCOm.Contains(":") Then
                        MessageBox.Show("No extension has been spefied for the source file. " & Environment.NewLine & "Must be in AddReferences:<extension> format")
                        Exit Sub

                    End If

                    ext = commandCOm.Split(":")(1)
                    If String.IsNullOrEmpty(ext) OrElse ext.Length <= 2 Then
                        MessageBox.Show("Invalid or no source extension specified. ")
                        Exit Sub

                    End If

                    If ext IsNot Nothing AndAlso Not (ext.StartsWith(".")) Then
                        ext = "." & ext


                        Dim fileID As Long
                        fileID = ppoData(0).mlObjectID1


                        If fileID <= 0 Then Exit Sub
                        Try
                            file = vault.GetObject(EdmObjectType.EdmObject_File, fileID)
                            If Not file.IsLocked Or Not (file.LockedByUserID = curUser.ID) Then
                                MessageBox.Show("The file is not checked out by you. ")
                                Exit Sub
                            End If


                            LaunchRefs(file, vault, ext)

                        Catch ex As Exception
                        End Try


                    End If
                End If


        End Select
     

        If poCmd.mlCmdID = 2 Then
            Dim setForm As New SettingsForm
            setForm.vault = vault
            setForm.ShowDialog()

        End If
        'Check the upper and lower bounds of the array.
        'Dim message As String
        'message = ""
        'Dim index As Long
        'index = LBound(ppoData)
        'Dim last As Long
        'last = UBound(ppoData)

        ''Get the paths of the affected files.
        'While index <= last
        '    message = message + ppoData(index).mbsStrData1 + vbLf
        '    index = index + 1
        'End While

        ''Display a message to the user.
        'message = "The following files were affected by a " + name + " hook:" + vbLf + message



    End Sub

    Private Sub LaunchRefs(file As IEdmFile8, vault As EdmVault5, ext As String)
        ErrorLogger._Vault = vault
        Dim fileop As PDMFile
        Dim datcon As New ConnectionSettings(vault)
        Dim dataop As New DataOps(datcon.connstring)
        If Not (dataop.returncode = DataOps.conxopts.connection_ok) Then
            MessageBox.Show("Failed to launch AddReferences. Failed to connect to SQL server. ")
            Exit Sub
        End If
        ErrorLogger.LogException(, , , file.Name, "LauchRefs method. ", "Connection to SQL OK.")

        Dim varlist As New List(Of String)
        varlist = dataop.variableList

        Dim varListFileName As New List(Of String) 'THis is the variables to which filename without extension will be copied.
        varListFileName = dataop.variableListfile




        Dim fileRefs As New List(Of FileAndFolder)
        If file Is Nothing Then
            Exit Sub
        End If


        ErrorLogger.LogException(, , , file.Name, "Got file object. ", "File accessed.")
        fileop = New PDMFile(file.ID, vault)
        fileRefs = fileop.getCustomReferences
        ErrorLogger.LogException(, , , file.Name, "Got references", " found " & fileRefs.Count & " Existing references")

        Try

            Dim setform As New AddrefsBox(vault, file)
            setform.Existinglist = fileRefs
            setform.targetExtension = ext
            Dim diagres As DialogResult
            diagres = setform.ShowDialog()

            If diagres = DialogResult.OK Then

                fileop.AddReferences(setform.FileList)
                fileop.copyVariables(setform.sourcefile, varlist)
                fileop.copyfileName(setform.sourcefile, varListFileName)
                If setform.checkfile = True Then
                    Try
                        file.UnlockFile(0, setform.comment, EdmUnlockFlag.EdmUnlock_Simple)
                    Catch ex As Exception
                        MessageBox.Show("Failed to check the file back in. " & Environment.NewLine & ex.Message)
                    End Try

                End If
            End If

        Catch ex As Exception
            ErrorLogger.LogException(vault, ex, "", file.Name, "LaunchRefs", "Error in LaunchRefs")
        End Try


    End Sub
End Class
