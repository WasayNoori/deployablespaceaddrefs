﻿Imports EdmLib

Imports Microsoft.VisualBasic
Imports System.Windows.Forms

Module Mainmod
    Sub main()

        Dim vault As New EdmVault5
        vault.LoginAuto("Appdemo", 0)

        'Dim setting As New SettingsForm
        'setting.vault = vault
        'setting.ShowDialog()

        Dim file As IEdmFile8
        Dim fileop As PDMFile
        Dim datcon As New ConnectionSettings(vault)
        Dim dataop As New DataOps(datcon.connstring)
        Dim varlist As New List(Of String)
        varlist = dataop.variableList
        ErrorLogger._Vault = vault

        ErrorLogger.LogException(, , , , "Logged in successfully", "Logging OK")


        Dim fileRefs As New List(Of FileAndFolder)
        Try

            file = vault.GetFileFromPath("d:\Appdemo\P091125.PDF")

            If Not file Is Nothing Then
                ErrorLogger.LogException(, , , file.Name, "GOt file object ok", "File accessed.")
                fileop = New PDMFile(file.ID, vault)
                fileRefs = fileop.getCustomReferences
                ErrorLogger.LogException(, , , file.Name, "Got references", " found " & fileRefs.Count & " references")

            End If

        Catch ex As Exception
            ErrorLogger.LogException(vault, ex, , , , "some message")
        End Try

        Dim setform As New AddrefsBox(vault, file)
        setform.Existinglist = fileRefs

        setform.targetExtension = ".PDF"
        Dim diagres As DialogResult


        diagres = setform.ShowDialog()

        If diagres = DialogResult.OK Then

            fileop.AddReferences(setform.FileList)
            fileop.copyVariables(setform.sourcefile, varlist)

            If setform.checkfile = True Then
                Try
                    file.UnlockFile(0, setform.comment, EdmUnlockFlag.EdmUnlock_Simple)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try

            End If
        End If
        'Dim consetting As New ConnectionSettings(vault)

        'Dim tempstring As String = "Data Source=wasay-ultra;Initial Catalog='Hawkware_EPDM_Addin';Integrated Security=False;user='sa';password='seattle123'"

        'Dim conops As New DataOps(consetting.connstring)

        'MessageBox.Show(conops.returncode)

        'Try
        '    Dim file As IEdmFile5
        '    file = vault.GetFileFromPath("c:\myfile.txt")
        '    file.LockFile(1, 0)
        'Catch ex As Exception
        '    ErrorLogger.LogException(vault, ex, , , , "some message")
        'End Try

    End Sub
End Module
