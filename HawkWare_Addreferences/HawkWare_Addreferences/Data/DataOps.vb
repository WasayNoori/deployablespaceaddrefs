﻿Imports System.Windows
Imports System.Data.SqlClient

Public Class DataOps
    Dim _connstring As String
    Dim mydb As SQLDataDataContext
    Private _user As String
    Private _password As String
    Private _server As String
    Private _varlist As List(Of String)

    Enum conxopts
        Database_Not_Found = 0
        No_Connection_String = 1
        Invalid_Server = 2
        Invalid_User_or_Password = 3
        Unknown_Error = 4
        Failed_to_Create_Database = 5
        connection_ok = 6

    End Enum
    Private _returncode As conxopts

    Public ReadOnly Property returncode As conxopts
        Get
            Return _returncode

        End Get
    End Property
    Public Property variableList As List(Of String)
        Get
            readVariables()
            Return _varlist

        End Get
        Set(value As List(Of String))
            _varlist = New List(Of String)
            _varlist = value
            saveVariables()

        End Set
    End Property
    Public Sub New(connstring As String)
        _connstring = connstring
        If String.IsNullOrEmpty(connstring) Then
            _returncode = conxopts.No_Connection_String
            Exit Sub
        End If
        Try
            mydb = New SQLDataDataContext(_connstring)
        Catch ex As Exception
            _returncode = conxopts.unknown_error
            Exit Sub

        End Try

        Try
            mydb.Connection.Open()
        Catch sqlex As SqlException
            Select Case sqlex.Number
                Case 18456
                    _returncode = conxopts.invalid_user_or_password
                    Exit Sub
                Case 53
                    _returncode = conxopts.invalid_server
                    Exit Sub
                Case 4060
                    Try
                        mydb.CreateDatabase()
                    Catch ex As Exception
                        _returncode = conxopts.Failed_to_Create_Database

                    End Try
                    Exit Sub


            End Select

        Catch ex As Exception
            _returncode = conxopts.Unknown_Error
            Exit Sub

        End Try
        _returncode = conxopts.connection_ok



        If mydb.DatabaseExists = False Then
            Try
                mydb.CreateDatabase()
            Catch ex As Exception
                _returncode = conxopts.failed_to_create_database

            End Try


        End If

    End Sub
    Private Sub readVariables()
        _varlist = New List(Of String)
        Dim q = (From a In mydb.Variables Select a).ToList
        For Each v As Variable In q
            _varlist.Add(v.VariableName)
        Next

    End Sub
    Private Sub saveVariables()

        'delete it
        For Each var As Variable In mydb.Variables
            mydb.Variables.DeleteOnSubmit(var)
        Next

        Try
            mydb.SubmitChanges()
        Catch ex As Exception
            MessageBox.Show("Errors encountered while clearing variables table. " & Environment.NewLine & ex.Message)
        End Try
        Dim tvar As Variable

        For Each v As String In _varlist
            tvar = New Variable
            tvar.VariableName = v
            mydb.Variables.InsertOnSubmit(tvar)

        Next
        Try
            mydb.SubmitChanges()

        Catch ex As Exception
            MessageBox.Show("Errors encountered while saving to variables table. " & Environment.NewLine & ex.Message)
        End Try
    End Sub
    Public Sub AddToLogtable(filename As String, user As String, action As String, exMsg As String, info As String)
        Dim nLog As New ErrorLog
        nLog.User = user
        nLog.Action = action
        nLog.File_Or_Folder_Name = filename
        nLog.Exeption = exMsg
        nLog.Info = info

        nLog.Date = Date.Now
        mydb.ErrorLogs.InsertOnSubmit(nLog)

        Try
            mydb.SubmitChanges()
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub
End Class
