﻿Imports EdmLib

Imports System.Windows.Forms

Public Class Addin
    Implements IEdmAddIn5
    Dim vault As EdmVault5
    Public Sub GetAddInInfo(ByRef poInfo As EdmAddInInfo, poVault As IEdmVault5, poCmdMgr As IEdmCmdMgr5) Implements IEdmAddIn5.GetAddInInfo
        poInfo.mbsAddInName = My.Application.Info.Title


        poInfo.mbsCompany = My.Application.Info.CompanyName


        poInfo.mbsDescription = My.Application.Info.Description

        poInfo.mlAddInVersion = My.Application.Info.Version.Major




        ' Minimum SolidWorks Enterprise PDM version needed for VB .Net Add-Ins is 6.4

        poInfo.mlRequiredVersionMajor = 6

        poInfo.mlRequiredVersionMinor = 4



        ' Register a menu command

        poCmdMgr.AddCmd(1, "Settings", EdmMenuFlags.EdmMenu_Administration)
        poCmdMgr.AddCmd(3, "Settings3", EdmMenuFlags.EdmMenu_Administration)
        poCmdMgr.AddCmd(2, "Ref Setting", EdmMenuFlags.EdmMenu_Nothing)
        'poCmdMgr.AddHook(EdmCmdType.EdmCmd_CardButton)


    End Sub

    Public Sub OnCmd(ByRef poCmd As EdmCmd, ByRef ppoData As Array) Implements IEdmAddIn5.OnCmd
        Try


            vault = New EdmVault5

            vault = poCmd.mpoVault

            If poCmd.meCmdType = EdmCmdType.EdmCmd_Menu Then
                MessageBox.Show("Menu pressed. ")
                MessageBox.Show(poCmd.mlCmdID)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        If poCmd.mlCmdID = 1 Then
            MessageBox.Show("launched. ")
            Try
                Dim sett As New SettingsForm
                sett.vault = vault
                sett.ShowDialog()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

        Dim act As New ActiavateME(vault)
        Dim file As IEdmFile8
        Dim curUser As IEdmUser10
        Dim pdmClass As New EpdmLists(vault)
        curUser = pdmClass.currentuser

        If act.isActivated = False Then
            System.Windows.MessageBox.Show("This add-in has not been activated. Please use Administrative settings to active. ")
            Exit Sub

        End If


        Dim commandCOm As String
        Dim ext As String
        If poCmd.meCmdType = EdmCmdType.EdmCmd_CardButton AndAlso Not (String.IsNullOrEmpty(poCmd.mbsComment)) AndAlso poCmd.mbsComment.ToLower.StartsWith("addref") Then
            commandCOm = poCmd.mbsComment
            If Not commandCOm.Contains(":") Then
                System.Windows.MessageBox.Show("No extension has been spefied for the source file. " & Environment.NewLine & "Must be in AddReferences:<extension> format")
                Exit Sub

            End If

            ext = commandCOm.Split(":")(1)
            If String.IsNullOrEmpty(ext) OrElse ext.Length <= 2 Then
                System.Windows.MessageBox.Show("Invalid or no source extension specified. ")
                Exit Sub

            End If

            If ext IsNot Nothing AndAlso Not (ext.StartsWith(".")) Then
                ext = "." & ext


                Dim fileID As Long
                fileID = ppoData(0).mlObjectID1
                If fileID <= 0 Then Exit Sub
                Try
                    file = vault.GetObject(EdmObjectType.EdmObject_File, fileID)
                    If Not file.IsLocked Or Not (file.LockedByUserID = curUser.ID) Then
                        MessageBox.Show("The file is not checked out by you. ")
                        Exit Sub
                    End If

                    LaunchRefs(file, vault, ext)

                Catch ex As Exception
                End Try


            End If
        End If


    End Sub

    Private Sub LaunchRefs(file As IEdmFile8, vault As EdmVault5, ext As String)
        ErrorLogger._Vault = vault
        Dim fileop As PDMFile
        Dim datcon As New ConnectionSettings(vault)
        Dim dataop As New DataOps(datcon.connstring)
        If Not (dataop.returncode = DataOps.conxopts.connection_ok) Then
            System.Windows.MessageBox.Show("Failed to launch AddReferences. Failed to connect to SQL server. ")
            Exit Sub
        End If
        ErrorLogger.LogException(, , , file.Name, "LauchRefs method. ", "Connection to SQL OK.")

        Dim varlist As New List(Of String)
        varlist = dataop.variableList

        Dim fileRefs As New List(Of FileAndFolder)
        If file Is Nothing Then
            Exit Sub
        End If


        ErrorLogger.LogException(, , , file.Name, "Got file object. ", "File accessed.")
        fileop = New PDMFile(file.ID, vault)
        fileRefs = fileop.getCustomReferences
        ErrorLogger.LogException(, , , file.Name, "Got references", " found " & fileRefs.Count & " Existing references")

        Try

            Dim setform As New AddrefsBox(vault, file)
            setform.Existinglist = fileRefs
            setform.targetExtension = ext
            Dim diagres As DialogResult
            diagres = setform.ShowDialog()

            If diagres = DialogResult.OK Then

                fileop.AddReferences(setform.FileList)
                fileop.copyVariables(setform.sourcefile, varlist)

                If setform.checkfile = True Then
                    Try
                        file.UnlockFile(0, setform.comment, EdmUnlockFlag.EdmUnlock_Simple)
                    Catch ex As Exception
                        System.Windows.MessageBox.Show("Failed to check the file back in. " & Environment.NewLine & ex.Message)
                    End Try

                End If
            End If

        Catch ex As Exception
            ErrorLogger.LogException(vault, ex, "", file.Name, "LaunchRefs", "Error in LaunchRefs")
        End Try


    End Sub
End Class
