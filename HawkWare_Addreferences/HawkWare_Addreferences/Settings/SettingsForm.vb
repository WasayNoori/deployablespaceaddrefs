﻿Imports EdmLib
Imports System.Windows.Forms

Public Class SettingsForm
    Public Property vault As EdmVault5
    Dim settingclass As VaultSettings

    Private Sub SettingsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = My.Application.Info.Title & " Settings"
        loaddata()


        If testConnection() = "OK" Then
            If TabControl1.TabPages.Contains(variablepage) = False Then
                TabControl1.TabPages.Add(variablepage)

            End If
            loadvariables()

        Else
            If TabControl1.TabPages.Contains(variablepage) Then
                TabControl1.TabPages.Remove(variablepage)
            End If
        End If


    End Sub
    Private Sub loadvariables()
        Dim consett As New ConnectionSettings(vault)
        Dim dbops As New DataOps(consett.connstring)
        Dim vars As New List(Of String)
        vars = dbops.variableList

        For Each v As String In vars
            lbvariables.Items.Add(v)

        Next
    End Sub

    Private Sub savevariables()
        Dim savedvars As New List(Of String)
        Dim consett As New ConnectionSettings(vault)
        Dim dbops As New DataOps(consett.connstring)
        For Each v As String In lbvariables.Items
            savedvars.Add(v)

        Next

        dbops.variableList = savedvars

    End Sub

    Private Sub loaddata()
        Try

            Dim conclass As New ConnectionSettings(vault)
            conclass.readData()

            tbuser.Text = conclass.sqluser
            tbpassword.Text = conclass.sqlpassword
            tbserver.Text = conclass.sqlserver

            Dim logging As String
            settingclass = New VaultSettings(vault)
            logging = settingclass.readVal("logging")
            If logging IsNot Nothing AndAlso logging.ToLower.Contains("true") Then
                CbLogging.Checked = True
            Else
                CbLogging.Checked = False

            End If

            Dim adminlogging As String
            adminlogging = settingclass.readVal("adminlogging")
            If Not String.IsNullOrEmpty(adminlogging) AndAlso adminlogging.ToLower.Contains("true") Then
                CbadminLogging.Checked = True
            Else
                CbadminLogging.Checked = False

            End If
        Catch ex As Exception
        End Try

    End Sub
    Private Sub savedata()
        Dim conclass As New ConnectionSettings(vault)
        conclass.sqluser = tbuser.Text
        conclass.sqlpassword = tbpassword.Text
        conclass.sqlserver = tbserver.Text
        conclass.saveData()
        settingclass = New VaultSettings(vault)

        If CbLogging.Checked Then
            settingclass.setValue("logging", "true")
        Else
            settingclass.setValue("logging", "false")
        End If

        If CbadminLogging.Checked Then
            settingclass.setValue("adminlogging", "true")
        Else
            settingclass.setValue("adminlogging", "false")

        End If
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        savedata()
        savevariables()

    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        If Not testConnection() = "OK" Then
            MessageBox.Show(testConnection)
        Else
            MessageBox.Show("Connection Successful.")
        End If
    End Sub
    Private Function testConnection() As String
        Dim username, password, server As String
        username = tbuser.Text
        password = tbpassword.Text
        server = tbserver.Text

        If String.IsNullOrEmpty(username) OrElse String.IsNullOrEmpty(password) OrElse String.IsNullOrEmpty(server) Then
            MessageBox.Show("Enter valid values. ")
            Return ""

        End If
        Dim consett As New ConnectionSettings(vault)
        Dim conx As String = consett.connstringfromvals(username, password, server)
        Dim dops As New DataOps(conx)
        If dops.returncode = DataOps.conxopts.connection_ok Then

            Return "OK"
        Else

            Return dops.returncode.ToString


        End If

    End Function

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        Dim abtbox As New AboutBox
        abtbox.ShowDialog()

    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnVarSetting.Click
        Dim existVars As New List(Of String)
        For Each lv As String In lbvariables.Items
            existVars.Add(lv)
        Next
        Dim diagres As DialogResult
        Dim varform As New variableselectform(vault)
        varform.selectedVariables = existVars
        diagres = varform.ShowDialog()
        If diagres = Windows.Forms.DialogResult.OK Then
            lbvariables.Items.Clear()

            For Each s As String In varform.selectedVariables
                lbvariables.Items.Add(s)

            Next
        End If

    End Sub
End Class