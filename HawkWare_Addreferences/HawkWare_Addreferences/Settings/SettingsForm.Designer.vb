﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SettingsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbserver = New System.Windows.Forms.TextBox()
        Me.tbpassword = New System.Windows.Forms.TextBox()
        Me.tbuser = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.CbadminLogging = New System.Windows.Forms.CheckBox()
        Me.CbLogging = New System.Windows.Forms.CheckBox()
        Me.variablepage = New System.Windows.Forms.TabPage()
        Me.btnVarSetting = New System.Windows.Forms.Button()
        Me.lbvariables = New System.Windows.Forms.ListBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.variablepage.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnTest)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.tbserver)
        Me.Panel1.Controls.Add(Me.tbpassword)
        Me.Panel1.Controls.Add(Me.tbuser)
        Me.Panel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(6, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(373, 173)
        Me.Panel1.TabIndex = 1
        '
        'btnTest
        '
        Me.btnTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTest.Location = New System.Drawing.Point(231, 24)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(120, 23)
        Me.btnTest.TabIndex = 5
        Me.btnTest.Text = "Connect"
        Me.ToolTip1.SetToolTip(Me.btnTest, "Test Connection")
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "SQL Settings"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 134)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Server"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Password"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "User"
        '
        'tbserver
        '
        Me.tbserver.Location = New System.Drawing.Point(91, 127)
        Me.tbserver.Name = "tbserver"
        Me.tbserver.Size = New System.Drawing.Size(260, 22)
        Me.tbserver.TabIndex = 2
        '
        'tbpassword
        '
        Me.tbpassword.Location = New System.Drawing.Point(91, 90)
        Me.tbpassword.Name = "tbpassword"
        Me.tbpassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbpassword.Size = New System.Drawing.Size(260, 22)
        Me.tbpassword.TabIndex = 1
        '
        'tbuser
        '
        Me.tbuser.Location = New System.Drawing.Point(91, 53)
        Me.tbuser.Name = "tbuser"
        Me.tbuser.Size = New System.Drawing.Size(260, 22)
        Me.tbuser.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.variablepage)
        Me.TabControl1.Location = New System.Drawing.Point(0, 27)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(476, 250)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.CbadminLogging)
        Me.TabPage1.Controls.Add(Me.CbLogging)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(468, 224)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "SQL Settings"
        '
        'CbadminLogging
        '
        Me.CbadminLogging.AutoSize = True
        Me.CbadminLogging.Location = New System.Drawing.Point(137, 185)
        Me.CbadminLogging.Name = "CbadminLogging"
        Me.CbadminLogging.Size = New System.Drawing.Size(132, 17)
        Me.CbadminLogging.TabIndex = 4
        Me.CbadminLogging.Text = "Enable Admin Logging"
        Me.ToolTip1.SetToolTip(Me.CbadminLogging, "Creates a text file of error logs on desktop" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "of Admin user.  Use only for diagno" & _
        "sis")
        Me.CbadminLogging.UseVisualStyleBackColor = True
        '
        'CbLogging
        '
        Me.CbLogging.AutoSize = True
        Me.CbLogging.Location = New System.Drawing.Point(19, 185)
        Me.CbLogging.Name = "CbLogging"
        Me.CbLogging.Size = New System.Drawing.Size(100, 17)
        Me.CbLogging.TabIndex = 2
        Me.CbLogging.Text = "Enable Logging"
        Me.ToolTip1.SetToolTip(Me.CbLogging, "Log Errors to SQL Database")
        Me.CbLogging.UseVisualStyleBackColor = True
        '
        'variablepage
        '
        Me.variablepage.Controls.Add(Me.btnVarSetting)
        Me.variablepage.Controls.Add(Me.lbvariables)
        Me.variablepage.Location = New System.Drawing.Point(4, 22)
        Me.variablepage.Name = "variablepage"
        Me.variablepage.Padding = New System.Windows.Forms.Padding(3)
        Me.variablepage.Size = New System.Drawing.Size(468, 224)
        Me.variablepage.TabIndex = 1
        Me.variablepage.Text = "Variables"
        Me.variablepage.UseVisualStyleBackColor = True
        '
        'btnVarSetting
        '
        Me.btnVarSetting.BackgroundImage = Global.HawkWare_Addreferences.My.Resources.Resources.plus_256
        Me.btnVarSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVarSetting.Location = New System.Drawing.Point(151, 29)
        Me.btnVarSetting.Name = "btnVarSetting"
        Me.btnVarSetting.Size = New System.Drawing.Size(22, 22)
        Me.btnVarSetting.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.btnVarSetting, "Add or remove variables")
        Me.btnVarSetting.UseVisualStyleBackColor = True
        '
        'lbvariables
        '
        Me.lbvariables.FormattingEnabled = True
        Me.lbvariables.Location = New System.Drawing.Point(8, 29)
        Me.lbvariables.Name = "lbvariables"
        Me.lbvariables.Size = New System.Drawing.Size(137, 173)
        Me.lbvariables.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(380, 286)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(505, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem1, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpToolStripMenuItem1
        '
        Me.HelpToolStripMenuItem1.Name = "HelpToolStripMenuItem1"
        Me.HelpToolStripMenuItem1.Size = New System.Drawing.Size(107, 22)
        Me.HelpToolStripMenuItem1.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'SettingsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(505, 321)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SettingsForm"
        Me.Text = "HawkWare™ Add Reference Settings"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.variablepage.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbserver As System.Windows.Forms.TextBox
    Friend WithEvents tbpassword As System.Windows.Forms.TextBox
    Friend WithEvents tbuser As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents CbLogging As System.Windows.Forms.CheckBox
    Friend WithEvents CbadminLogging As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnTest As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents variablepage As System.Windows.Forms.TabPage
    Friend WithEvents btnVarSetting As System.Windows.Forms.Button
    Friend WithEvents lbvariables As System.Windows.Forms.ListBox
End Class
