﻿Imports EdmLib
Imports System.Windows

Public Class ErrorLogger
    Public Shared _Vault As EdmVault5
    Dim mydb As MyDataDataContext
    Dim _fileid As String
    Dim _fileName As String
    Dim _action As String
    Dim _msg As String


    Public Shared Sub LogException(Optional vault As EdmVault5 = Nothing, Optional ex As Exception = Nothing, Optional user As String = "", Optional filename As String = "", Optional action As String = "", Optional message As String = "")
        'see if logging is on.
        Dim logging As String
        Dim adminLogging As String
        Dim vsettings As New VaultSettings(_Vault)
        Dim curUser As IEdmUser10
        Dim pdmlist As New EpdmLists(_Vault)
        curUser = pdmlist.currentuser
        If user = "" Then
            user = curUser.Name
        End If

        logging = vsettings.readVal("logging")
        adminLogging = vsettings.readVal("adminlogging")

        If logging IsNot Nothing AndAlso logging.ToLower.Contains("true") Then
            Dim consetting As New ConnectionSettings(_Vault)
            Dim dbop As New DataOps(consetting.connstring)
            If dbop.returncode = DataOps.conxopts.connection_ok Then
                If Not ex Is Nothing Then
                    If ex.InnerException IsNot Nothing Then
                        dbop.AddToLogtable(filename, user, action, ex.InnerException.Message, message)
                    Else
                        dbop.AddToLogtable(filename, user, action, ex.ToString, message)
                    End If
                Else
                    dbop.AddToLogtable(filename, user, action, "", message)

                    'do nothing

                End If
            End If

        End If
        If adminLogging IsNot Nothing AndAlso adminLogging.ToLower.Contains("true") Then
            LogAdminError(_Vault, ex, user, filename, action, message)
        End If
    End Sub

    Public Shared Sub LogAdminError(vault As EdmVault5, ex As Exception, Optional user As String = "",
                                    Optional filename As String = "",
                                    Optional action As String = "", Optional message As String = "")
        Try
            'this method is executed to log errors on the desktop of Admin user when the SQL logging is not working
            Dim usercls As New EpdmLists(vault)
            Dim curUser As IEdmUser10
            Dim errmsg As String
            If ex IsNot Nothing AndAlso ex.InnerException IsNot Nothing Then
                errmsg = ex.InnerException.Message
            ElseIf ex IsNot Nothing AndAlso ex.InnerException Is Nothing Then
                errmsg = ex.Message
            End If
            Dim logfile As String = My.Application.Info.ProductName.ToString & "_log.txt"
            curUser = usercls.currentuser
            If curUser IsNot Nothing AndAlso curUser.Name.ToLower = "admin" Then
                Dim deskdir As String = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                Dim strWriter As New System.IO.StreamWriter(IO.Path.Combine(deskdir, logfile), True)
                strWriter.WriteLine(DateTime.Now.ToString & vbTab & filename & vbTab & user & vbTab & action & vbTab & errmsg)
                strWriter.Close()
                strWriter.Dispose()

            End If
        Catch ex2 As Exception
        End Try

    End Sub
  
End Class
