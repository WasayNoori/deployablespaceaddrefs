﻿Imports EdmLib

Public Class EpdmLists
    Dim thisVault As EdmVault5
    Dim statelist As List(Of IEdmState7)
    Dim workflowList As List(Of IEdmWorkflow6)
    Dim UserList As List(Of IEdmUser10)
    Dim groupList As List(Of IEdmUserGroup8)
    Dim variableLIst As List(Of IEdmVariable5)
    Private _errorout As String


    Sub New(ByVal vault As EdmVault5)
        thisVault = New EdmVault5
        thisVault = vault


    End Sub



    Sub StateListAll()

    End Sub
#Region "Properties"
    Public ReadOnly Property wfList As List(Of IEdmWorkflow6)
        Get
            Return getWorkflow(thisVault)

        End Get
    End Property
    Public ReadOnly Property getVarList As List(Of IEdmVariable5)
        Get
            Return GetAllVariables()

        End Get
    End Property
    Public ReadOnly Property GetStateListByWFName(ByVal wfname As String) As List(Of IEdmState7)
        Get
            Return getStateFromWFName(wfname)

        End Get
    End Property
    Public ReadOnly Property getGroupList() As List(Of IEdmUserGroup8)
        Get
            Return getAllGroups()

        End Get
    End Property
    Public ReadOnly Property GetAllusersList As List(Of IEdmUser10)
        Get
            Return getAllUSers()

        End Get
    End Property
    Public ReadOnly Property GetStateList(ByVal wf As IEdmWorkflow6) As List(Of IEdmState7)
        Get
            Return getStateFromWF(wf)

        End Get
    End Property

    Public ReadOnly Property UserListByGroupName(ByVal groupname As String) As List(Of IEdmUser10)
        Get
            Return getUSersGromGroupName(groupname)

        End Get
    End Property

    Public ReadOnly Property getCategoryList() As List(Of IEdmCategory6)
        Get
            Return getCategories()
        End Get
    End Property

    Public ReadOnly Property ErrorOut As String
        Get
            Return _errorout

        End Get

    End Property

    Public ReadOnly Property UserByName(username As String) As IEdmUser10
        Get
            Return GetUser(username)
        End Get
    End Property
    Public ReadOnly Property currentuser As IEdmUser10
        Get
            Return getCurrentUser()

        End Get
    End Property
#End Region

    Private Function getWorkflow(ByVal vault As EdmVault5) As List(Of IEdmWorkflow6)
        On Error GoTo errhand
        Dim WorkflowMgr As IEdmWorkflowMgr6
        Dim wfList As New List(Of IEdmWorkflow6)


        WorkflowMgr = vault

        Dim Pos As IEdmPos5

        Pos = WorkflowMgr.GetFirstWorkflowPosition




        While (Not Pos.IsNull)
            wfList.Add(WorkflowMgr.GetNextWorkflow(Pos))


        End While

        Return wfList
errhand:
        Return wfList

    End Function
    Private Function getStateFromWF(ByVal wf As IEdmWorkflow6) As List(Of IEdmState7)
        Dim retStates As New List(Of IEdmState7)

        Dim pos As IEdmPos5
        pos = wf.GetFirstStatePosition

        While (Not pos.IsNull)

            retStates.Add(wf.GetNextState(pos))


        End While
        Return retStates


    End Function
    Private Function getStateFromWFName(ByVal wfName As String) As List(Of IEdmState7)
        On Error GoTo errhand

        Dim retStates As New List(Of IEdmState7)
        Dim mywf As IEdmWorkflow6
        Dim pos As IEdmPos5
        Dim pos2 As IEdmPos5
        Dim myState As IEdmState6
        Dim wfMgr As IEdmWorkflowMgr6
        wfMgr = thisVault
        pos = wfMgr.GetFirstWorkflowPosition

        While Not pos.IsNull
            mywf = wfMgr.GetNextWorkflow(pos)
            If mywf.Name.ToLower = wfName.ToLower Then
                pos2 = mywf.GetFirstStatePosition
                While Not pos2.IsNull
                    myState = mywf.GetNextState(pos2)
                    retStates.Add(myState)
                End While
            End If
        End While
        Return retStates
errhand:
        Return retStates

    End Function
    Private Function getAllUSers() As List(Of IEdmUser10)
        Dim retLIst As New List(Of IEdmUser10)

        Dim userMgr As IEdmUserMgr8
        userMgr = thisVault

        Dim pos As IEdmPos5
        pos = userMgr.GetFirstUserPosition
        While Not pos.IsNull
            retLIst.Add(userMgr.GetNextUser(pos))
        End While
        Return retLIst

    End Function
    Private Function getAllGroups() As List(Of IEdmUserGroup8)
        On Error GoTo errhand

        Dim retLIst As New List(Of IEdmUserGroup8)
        Dim userMgr As IEdmUserMgr8
        userMgr = thisVault

        Dim uG As IEdmUserGroup8
        Dim pos As IEdmPos5
        pos = userMgr.GetFirstUserGroupPosition
        While Not pos.IsNull
            retLIst.Add(userMgr.GetNextUserGroup(pos))

        End While
        Return retLIst

errhand:
        Return retLIst

    End Function
    Private Function GetAllVariables() As List(Of IEdmVariable5)
        On Error GoTo errhand

        Dim retLIst As New List(Of IEdmVariable5)
        Dim varMgr As IEdmVariableMgr5
        varMgr = thisVault
        Dim pos As IEdmPos5
        pos = varMgr.GetFirstVariablePosition
        While Not pos.IsNull
            retLIst.Add(varMgr.GetNextVariable(pos))

        End While
        Return retLIst
errhand:
        Return retLIst

    End Function
    Private Function getUSersGromGroupName(ByVal grpName As String) As List(Of IEdmUser10)
        On Error GoTo errhand

        Dim retlist As New List(Of IEdmUser10)

        Dim pos As IEdmPos5
        Dim userMgr As IEdmUserMgr8
        Dim group As IEdmUserGroup8
        userMgr = thisVault
        Dim pos2 As IEdmPos5

        pos = userMgr.GetFirstUserGroupPosition
        While Not pos.IsNull
            group = userMgr.GetNextUserGroup(pos)

            If group.Name.ToLower = grpName.ToLower Then
                pos2 = group.GetFirstUserPosition
                While Not pos2.IsNull
                    retlist.Add(group.GetNextUser(pos2))
                End While

            End If
        End While
        Return retlist
errhand:
        Return retlist

    End Function
    Private Function getCategories() As List(Of IEdmCategory6)
        On Error GoTo errhand

        Dim retList As New List(Of IEdmCategory6)


        Dim Categories As IEdmCategoryMgr6

        Categories = thisVault.CreateUtility(EdmUtility.EdmUtil_CategoryMgr)


        Dim Pos As IEdmPos5

        Pos = Categories.GetFirstCategoryPosition

        Dim Category As IEdmCategory6





        While Not Pos.IsNull

            Category = Categories.GetNextCategory(Pos)

            retList.Add(Category)


        End While


        Return retList
errhand:
        Return retList



    End Function

    Private Function getCurrentUser() As IEdmUser10
        Try
            Dim userMgr As IEdmUserMgr5
            userMgr = thisVault.CreateUtility(EdmUtility.EdmUtil_UserMgr)
            Dim user As IEdmUser10

            user = CType(userMgr.GetLoggedInUser(), IEdmUser10)
            Return user
        Catch ex As Exception
            ErrorLogger.LogException(thisVault, ex, , , , "Getting current user")
            Return Nothing
        End Try


    End Function

    Private Function GetUser(userName As String) As IEdmUser10
        If userName Is Nothing Then Return Nothing
        If thisVault Is Nothing Then Return Nothing

        Try
            userName = userName.ToString

            Dim userID As Integer = 0
            Dim retUser As IEdmUser10
            Dim userMgr As IEdmUserMgr8
            userMgr = thisVault.CreateUtility(EdmUtility.EdmUtil_UserMgr)

            Dim user As IEdmUser10

            Try
                retUser = userMgr.GetUser(userName)
                If retUser Is Nothing Then
                    Dim pos As IEdmPos5
                    pos = userMgr.GetFirstUserPosition
                    While Not pos.IsNull
                        user = CType(userMgr.GetNextUser(pos), IEdmUser10)
                        If user.FullName.ToLower = userName.ToLower Then
                            Return user

                        End If

                    End While
                    Return Nothing


                Else
                    Return retUser
                End If

            Catch ex As Exception
                ErrorLogger.LogException(thisVault, ex, , , , "Getting User from " & userName)
                Return Nothing
            End Try

            If Not retUser Is Nothing Then
                Return retUser
            Else
                Return Nothing
            End If

        Catch ex As Exception
            ErrorLogger.LogException(thisVault, ex, , , , "Getting User from " & userName)
            Return Nothing

        End Try

    End Function
End Class
