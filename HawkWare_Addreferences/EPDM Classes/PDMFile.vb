﻿Imports EdmLib

Public Class PDMFile
    Dim _vault As IEdmVault10
    Dim _file As IEdmFile8
    Dim _fold As IEdmFolder7
    Dim _parentfolds As List(Of IEdmFolder7)
    Dim _refPaths As List(Of String)
    Public Sub New(fileid As Integer, vault As EdmVault5)
        If Not vault Is Nothing Then
            Try
                _vault = CType(vault, IEdmVault10)
                _file = _vault.GetObject(EdmObjectType.EdmObject_File, fileid)
            Catch ex As Exception
            End Try


        End If
    End Sub

    Public Sub New(filepath As String, vault As EdmVault5)
        If Not vault Is Nothing Then
            Try
                _vault = CType(vault, IEdmVault10)
                _file = _vault.GetFileFromPath(filepath, _fold)
            Catch ex As Exception
            End Try


        End If
    End Sub


#Region "Properties"
    Public ReadOnly Property ParentFolders As List(Of IEdmFolder7)
        Get
            ListShares()
            Return _parentfolds

        End Get
    End Property
    Public ReadOnly Property variableValue(varname As String, Optional configuration As String = "")
        Get
            Return getVarValue(varname, configuration)
        End Get
    End Property
    Public ReadOnly Property file As IEdmFile8
        Get
            Return _file

        End Get
    End Property

    Public ReadOnly Property fileworkflow As IEdmWorkflow6
        Get
            Return getWorkflow()

        End Get
    End Property
    Public ReadOnly Property getCustomReferences As List(Of FileAndFolder)
        Get
            Return ShowFileRefs(_file)

        End Get
    End Property
    Public ReadOnly Property GetCustomReferencepaths As List(Of String)
        Get
            Return ReferenceFilePaths()

        End Get
    End Property

#End Region

#Region "Methods"
    Public Sub AddReferences(CustRefsList As List(Of FileAndFolder))
        ErrorLogger.LogException(, , , , "AddReferences in PDMFILE", "Adding CustRefList of count " & CustRefsList.Count)
        'check out the file if not already checked out
        'not for this app


        Dim mainFile As IEdmEnumeratorCustomReference6
        mainFile = CType(_file, IEdmEnumeratorCustomReference6)

        For Each ff As FileAndFolder In CustRefsList
            Try
                mainFile.AddReference(ff.FileID, ff.FoldID)
            Catch ex As Exception
                ErrorLogger.LogException(, ex, , , "MainFile.AddReference error", "")
            End Try


        Next
    End Sub
    Public Sub copyfileName(sourceFile As IEdmFile8, varlist As List(Of String))
        Dim filename As String
        If sourceFile Is Nothing Then
            Exit Sub
        End If
        filename = IO.Path.GetFileNameWithoutExtension(sourceFile.Name)

        For Each p As String In varlist
            Try
                setvarvalue(p, filename)
            Catch ex As Exception
            End Try


        Next
    End Sub
    Public Sub copyVariables(sourceFile As IEdmFile8, proplist As List(Of String))
        Dim val As String
        For Each p As String In proplist
            Try
                val = getVarValue(sourceFile, p)
                If Not String.IsNullOrEmpty(val) Then
                    setvarvalue(p, val)
                End If
            Catch ex As Exception
                ErrorLogger.LogException(, ex, , sourceFile.Name, "CopyVariables sub ", "Error in copying properties")
            End Try


        Next
    End Sub
#End Region

    Private Sub ListShares()
        _parentfolds = New List(Of IEdmFolder7)
        If _file Is Nothing Then Exit Sub

        Try
            Dim pos As IEdmPos5
            Dim fold7 As IEdmFolder7
            pos = _file.GetFirstFolderPosition

            Dim folder As IEdmFolder5


            While Not pos.IsNull
                Try
                    folder = _file.GetNextFolder(pos)
                    If Not folder Is Nothing Then
                        fold7 = CType(folder, IEdmFolder7)
                        _parentfolds.Add(fold7)
                    End If
                Catch ex As Exception
                    ErrorLogger.LogException(_vault, ex, , , , "Error in getting share folders")
                End Try

            End While
        Catch ex As Exception
            ErrorLogger.LogException(_vault, ex, , , , "Error in getting share folders")
        End Try


    End Sub
    Private Function getVarValue(sourcefile As IEdmFile8, varname As String, Optional configName As String = "") As String
        If sourcefile Is Nothing Then Return Nothing
        Dim ext As String

        If configName = "" Then
            ext = IO.Path.GetExtension(sourcefile.Name).ToLower
            If ext = (".sldprt") Or ext = ".sldasm" Or ext = ".slddrw" Then
                configName = "@"
            End If
        End If

        Dim enumvar As IEdmEnumeratorVariable8
        Dim valObj As Object

        Try
            enumvar = sourcefile.GetEnumeratorVariable
            If enumvar.GetVar(varname, configName, valObj) Then
                Return valObj.ToString
            Else
                Return Nothing
            End If

        Catch ex As Exception
            enumvar = Nothing

            Return Nothing

        End Try
    End Function
    Private Function getWorkflow() As IEdmWorkflow6
        If _file Is Nothing Then Return Nothing
        Dim filestate As IEdmState7
        Dim wf As IEdmWorkflow6

        Try
            filestate = _file.CurrentState
            wf = _vault.GetObject(EdmObjectType.EdmObject_Workflow, filestate.WorkflowID)
            Return wf

        Catch ex As Exception
            ErrorLogger.LogException(_vault, ex, , , , "Getting workflow lists")
            Return Nothing

        End Try
    End Function
    Private Sub setvarvalue(varname As String, varvalue As String, Optional configName As String = "")
        If _file Is Nothing Then Exit Sub

        Dim ext As String

        If configName = "" Then
            ext = IO.Path.GetExtension(_file.Name).ToLower
            If ext = (".sldprt") Or ext = ".sldasm" Or ext = ".slddrw" Then
                configName = "@"
            End If
        End If
        Dim enumvar As IEdmEnumeratorVariable8
        Dim valObj As Object = Nothing
        enumvar = _file.GetEnumeratorVariable

        Try

            enumvar.SetVar(varname, configName, varvalue)
            enumvar.CloseFile(True)
            enumvar = Nothing

        Catch ex As Exception
            ErrorLogger.LogException(_vault, ex, "", _file.Name, "Copying variable " & varname, "Error while copying variable")
            If Not enumvar Is Nothing Then
                enumvar.CloseFile(True)
            End If
        End Try
        If Not enumvar Is Nothing Then
            enumvar.CloseFile(True)
        End If
    End Sub

    Private Function ReferenceFilePaths() As List(Of String)
        _refPaths = New List(Of String)
        Try
            Dim thisfile As IEdmFile8
            For Each ff As FileAndFolder In ShowFileRefs(_file)
                thisfile = _vault.GetObject(EdmObjectType.EdmObject_File, ff.FileID)
                If Not thisfile Is Nothing Then
                    _refPaths.Add(thisfile.Name)
                End If

            Next
        Catch ex As Exception
            Return _refPaths
        End Try

        Return _refPaths

    End Function
    Private Function ShowFileRefs(file As IEdmFile5) As List(Of FileAndFolder)
        On Error GoTo ErrHand

        Dim enumRef As IEdmEnumeratorCustomReference5
        Dim fileFold As FileAndFolder
        Dim retList As New List(Of FileAndFolder)

        enumRef = file
        Dim pos As IEdmPos5
        pos = enumRef.GetFirstRefPosition
        Dim fileID As Long
        Dim folderID As Long
        Dim path As String
        Dim message As String

        While Not pos.IsNull
            path = enumRef.GetNextRef(pos, fileID, folderID)
            fileFold = New FileAndFolder
            fileFold.FileID = fileID
            fileFold.FoldID = folderID
            retList.Add(fileFold)

        End While
        Return retList

        Exit Function

ErrHand:
        Dim ename As String
        Dim edesc As String
        file.Vault.GetErrorString(Err.Number, ename, edesc)
        ErrorLogger.LogException(_vault, , , file.Name, "Getting Custom References", Err.Number & " " & edesc)

    End Function
    Private Function ShowFileRefPaths(file As IEdmFile5) As List(Of String)
        Dim retlist As New List(Of String)
        Dim fileObj As IEdmFile8
        Dim FileRefObjs As New List(Of FileAndFolder)
        FileRefObjs = ShowFileRefs(file)
        For Each f As FileAndFolder In FileRefObjs
            Try
                fileObj = _vault.GetObject(EdmObjectType.EdmObject_File, f.FileID)
                If Not fileObj Is Nothing Then
                    retlist.Add(fileObj.Name)

                End If
            Catch ex As Exception
            End Try


        Next
    End Function

End Class

Public Class FileAndFolder
    Public Property FileID As Integer
    Public Property FoldID As Integer

End Class


