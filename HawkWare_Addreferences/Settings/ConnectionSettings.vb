﻿Imports EdmLib

Public Class ConnectionSettings
    Private _sqlUser As String
    Private _sqlpassword As String
    Private _sqlserver As String

    Private _Vault As EdmVault5
    Private _connectionString As String

    Sub New(vault As EdmVault5)
        If vault IsNot Nothing Then
            _Vault = vault
        End If

    End Sub

    Public Property sqluser As String
        Get
            Return _sqlUser
        End Get
        Set(value As String)
            _sqlUser = value

        End Set
    End Property

    Public ReadOnly Property connstring As String
        Get
            readData()
            Return getconnstring()


        End Get
    End Property

    Public ReadOnly Property connstringfromvals(username As String, password As String, server As String)
        Get
            _sqlUser = username
            _sqlpassword = password
            _sqlserver = server

            Return getConnstringfromValues()

        End Get
    End Property
    Public Property sqlpassword As String
        Get
            Dim crypthis As New Crypter("rhs1000")
            Return crypthis.DecryptData(_sqlpassword)
        End Get
        Set(value As String)
            Dim crypthis As New Crypter("rhs1000")
            _sqlpassword = crypthis.EncryptData(value)
          
        End Set
    End Property

    Public Property sqlserver As String
        Get
            Return _sqlserver
        End Get
        Set(value As String)
            _sqlserver = value

        End Set
    End Property

    Public Sub readData()
        If _Vault Is Nothing Then
            Exit Sub
        End If
        Dim settingClass As New VaultSettings(_Vault)
        _sqlUser = settingClass.readVal("sqluser")
        _sqlpassword = settingClass.readVal("sqlpassword")
        _sqlserver = settingClass.readVal("sqlserver")


    End Sub

    Public Sub saveData()
        If _Vault Is Nothing Then
            Exit Sub
        End If
        Dim settingClass As New VaultSettings(_Vault)

        If _sqlUser IsNot Nothing Then
            settingClass.setValue("sqluser", _sqlUser)
        End If
        If _sqlpassword IsNot Nothing Then
            settingClass.setValue("sqlpassword", _sqlpassword)
        End If
        If _sqlserver IsNot Nothing Then
            settingClass.setValue("sqlserver", _sqlserver)
        End If
    End Sub

    Private Function getconnstring() As String
        readData()
        Dim dbname As String
        dbname = My.Application.Info.ProductName
        Dim crypthis As New Crypter("rhs1000")
        _sqlpassword = crypthis.DecryptData(_sqlpassword)

        Return "Data Source=" + _sqlserver + ";Initial Catalog='" + dbname + "';Integrated Security=False;user='" + _sqlUser + _
                  "';password='" + _sqlpassword + "'"

    End Function

    Private Function getConnstringfromValues() As String
        Dim dbname As String
        dbname = My.Application.Info.ProductName
        Return "Data Source=" + _sqlserver + ";Initial Catalog='" + dbname + "';Integrated Security=False;user='" + _sqlUser + _
                 "';password='" + _sqlpassword + "'"
    End Function
End Class
